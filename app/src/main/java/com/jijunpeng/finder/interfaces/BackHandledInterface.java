package com.jijunpeng.finder.interfaces;

/**
 * Created by jijunpeng on 2016/12/5.
 *
 * @author Ji Junpeng
 */

public interface BackHandledInterface {
    boolean onBackPressed();
}
