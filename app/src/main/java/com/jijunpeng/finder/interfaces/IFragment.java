package com.jijunpeng.finder.interfaces;

import android.support.annotation.LayoutRes;

/**
 * Created by jijunpeng on 16/11/19.
 *
 * @author Ji Junpeng
 */

public interface IFragment {
    void beforeCreateFragmentView();

    void afterCreateFragmentView();

    @LayoutRes
    int getFragmentViewLayout();

}
