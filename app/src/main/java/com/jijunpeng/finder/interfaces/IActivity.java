package com.jijunpeng.finder.interfaces;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;

/**
 * Created by jijunpeng on 16/11/19.
 *
 * @author Ji Junpeng
 */

public interface IActivity {
    void beforeSetContentView();

    void afterSetContentView();

    @LayoutRes
    int getContentViewLayout();

    @IdRes
    int getToolbarViewId();
}
