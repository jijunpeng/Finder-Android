package com.jijunpeng.finder.ui;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jijunpeng.finder.R;
import com.jijunpeng.finder.adapter.FileListAdapter;
import com.jijunpeng.finder.interfaces.BackHandledInterface;
import com.jijunpeng.finder.utils.Util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_NAME;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_OBJ;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_SIZE;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_TYPE;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_TYPE_DIR;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_TYPE_FILE;
import static com.jijunpeng.finder.adapter.FileListAdapter.FILE_UPDATE_TIME;

/**
 * Created by jijunpeng on 16/11/19.
 *
 * @author Ji Junpeng
 */

public class MainFragment extends Fragment implements BackHandledInterface {

    private static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.file_list)
    protected ListView mFileList;
    @BindView(R.id.path)
    protected TextView mPathView;
    protected FileListAdapter mAdapter;
    private boolean mShowInvisible = false;
    private LinkedList<String> mHistory;//访问的文件夹历史记录

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        mHistory = new LinkedList<>();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_invisible:
                mShowInvisible = !mShowInvisible;
                if (mShowInvisible) {
                    item.setTitle("不显示隐藏文件");
                    Toast.makeText(getActivity(), "已经显示隐藏文件", Toast.LENGTH_SHORT).show();
                } else {
                    item.setTitle("显示隐藏文件");
                    Toast.makeText(getActivity(), "已经不显示隐藏文件", Toast.LENGTH_SHORT).show();
                }
                mAdapter.setShowHiddenFiles(mShowInvisible);
                mAdapter.notifyDataSetChanged();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        File file = Environment.getExternalStorageDirectory();
        mHistory.add(file.getAbsolutePath());
        initShowAdapter(file);
    }

    private void initShowAdapter(File fileToShow) {
        String path = fileToShow.getAbsolutePath();
        mPathView.setText(path);
        mAdapter = new FileListAdapter(getActivity());
        File[] files = fileToShow.listFiles();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss", Locale.getDefault());
        for (File file : files) {
            HashMap<String, Object> map = new HashMap<>();
            String fileName = file.getName();
            String fileSize;
            String fileType;
            if (file.isDirectory()) {
                fileSize = "目录 " + file.list().length + "个文件";
                fileType = FILE_TYPE_DIR;
            } else {
                fileSize = Util.prettySize(file.length());
                fileType = FILE_TYPE_FILE;
            }
            String fileUpdateTime = sdf.format(file.lastModified());
            map.put(FILE_NAME, fileName);
            map.put(FILE_SIZE, fileSize);
            map.put(FILE_UPDATE_TIME, fileUpdateTime);
            map.put(FILE_TYPE, fileType);
            map.put(FILE_OBJ, file);
            mAdapter.addData(map);
        }
        mFileList.setAdapter(mAdapter);
    }

    @OnItemClick(R.id.file_list)
    void onFileItemClick(int position) {
        HashMap<String, Object> map = mAdapter.getItem(position);
        File file = (File) map.get(FILE_OBJ);
        if (file.isDirectory()) {
            mHistory.add(file.getAbsolutePath());
            initShowAdapter(file);
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mHistory.size() <= 1) {
            return false;
        }
        mHistory.removeLast();
        String previousPath = mHistory.getLast();
        File file = new File(previousPath);
        initShowAdapter(file);
        return true;
    }
}
