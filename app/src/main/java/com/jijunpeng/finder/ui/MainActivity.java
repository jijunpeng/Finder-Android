package com.jijunpeng.finder.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jijunpeng.finder.R;
import com.jijunpeng.finder.interfaces.BackHandledInterface;

import java.util.List;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_content, new MainFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment.isVisible() && fragment instanceof BackHandledInterface) {
                BackHandledInterface backHandledInterface = (BackHandledInterface) fragment;
                backHandledInterface.onBackPressed();
                return;
            }
        }
        super.onBackPressed();
    }
}
