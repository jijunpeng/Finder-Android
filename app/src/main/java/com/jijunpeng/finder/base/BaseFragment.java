package com.jijunpeng.finder.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jijunpeng.finder.interfaces.IFragment;

import butterknife.ButterKnife;

/**
 * Created by jijunpeng on 16/11/19.
 *
 * @author Ji Junpeng
 */

public class BaseFragment extends Fragment implements IFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        beforeCreateFragmentView();
        if (getFragmentViewLayout() == 0) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        View view = inflater.inflate(getFragmentViewLayout(), container, false);
        ButterKnife.bind(this, view);
        afterCreateFragmentView();
        return view;
    }


    @Override
    public void beforeCreateFragmentView() {

    }

    @Override
    public void afterCreateFragmentView() {

    }

    @Override
    public int getFragmentViewLayout() {
        return 0;
    }
}
