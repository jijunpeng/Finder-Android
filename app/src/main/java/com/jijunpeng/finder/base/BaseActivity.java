package com.jijunpeng.finder.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jijunpeng.finder.interfaces.IActivity;

import butterknife.ButterKnife;

/**
 * Created by jijunpeng on 16/11/19.
 * <p>
 * BaseActivity 做所有Activity中统一的工作
 * 1.加入butterknife;
 *
 * @author Ji Junpeng
 */

public class BaseActivity extends AppCompatActivity implements IActivity {
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getContentViewLayout() != 0) {
            beforeSetContentView();
            setContentView(getContentViewLayout());
            ButterKnife.bind(this);
            if (getToolbarViewId() != 0) {
                mToolbar = (Toolbar) findViewById(getToolbarViewId());
                setSupportActionBar(mToolbar);
            }
            afterSetContentView();
        }
    }

    @Override
    public void beforeSetContentView() {

    }

    @Override
    public void afterSetContentView() {

    }

    @Override
    public int getContentViewLayout() {
        return 0;
    }

    @Override
    public int getToolbarViewId() {
        return 0;
    }
}
