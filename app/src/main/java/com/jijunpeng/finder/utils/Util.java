package com.jijunpeng.finder.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * Created by jijunpeng on 16/11/13.
 *
 * @author Ji Junpeng
 */

public class Util {
    public static long[] getExternalStorageSize() {
        File file = Environment.getExternalStorageDirectory();
        return new long[]{file.getTotalSpace(), file.getUsableSpace()};
    }

    public static long[] getSystemStorageSize() {
        File file = Environment.getRootDirectory();
        return new long[]{file.getTotalSpace(), file.getUsableSpace()};
    }

    public static String prettySize(long size) {
        int unitNum = 1000;
        String result = null;
        if (size < unitNum) {
            return size + "B";
        }
        float newSize = size;

        String[] units = {"KB", "MB", "GB", "TB"};
        for (String unit : units) {
            newSize = newSize / (float) unitNum;
            if (newSize < unitNum) {
                result = String.format(Locale.getDefault(), "%.02f", newSize) + unit;
                break;
            }
        }
        return result;
    }

    public static List<ApplicationInfo> getInstalledAppsNum(Context context) {
        int flag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            flag = PackageManager.MATCH_UNINSTALLED_PACKAGES;
        } else {
            flag = PackageManager.GET_UNINSTALLED_PACKAGES;
        }
        List<ApplicationInfo> installedApplications = context.getPackageManager().getInstalledApplications(flag);

        return installedApplications;
    }

}
